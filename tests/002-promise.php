<?php
declare(ticks=30);

use function F2\asserty;
use function F2\{defer, await};

require(__DIR__."/../vendor/autoload.php");

$resolveWhen = microtime(true) + 0.3;

$promise = new F2\Promise\Promise($resolver = function ($yes, $no) use ($resolveWhen, &$resolver) {
    if (microtime(true) > $resolveWhen) {
        $yes("Resolved by wait function");
    } else {
        defer($resolver, $yes, $no);
    }
});

$promise->then(function($result) {
    echo "late then success: $result\n";
    asserty(true);
}, function($reason) {
    echo "late then failure: $reason\n";
    asserty(false, "Promise failed");
});

$result = await($promise);

var_dump("RESULT", $result);
