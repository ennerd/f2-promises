<?php
declare(ticks=30);

use function F2\asserty;
use function F2\{defer, await, sleep, readable, writable};

require(__DIR__."/../vendor/autoload.php");

/*
defer(function() {

echo "------------------------------ no n modifier\n";
    echo microtime(true)."\n";
    $fp = yield fopen('/tmp/rwtest.tmp', 'c+b');
echo "------------------------------ n modifier\n";
    echo microtime(true)."\n";
    $fp = yield fopen('/tmp/rwtest.tmp', 'c+bn');

});
die();
*/
defer(function() {

    echo microtime(true)."\n";
    $fp = yield fopen('http://www.ennerd.com', 'rb');
    echo microtime(true)."\n";
    $fp = yield fopen('http://www.ennerd.com', 'rbn');

});
die();


defer(function() {
    echo "We've started\n";
    $t = microtime(true);
    $fp = yield writable(fopen('http://www.ennerd.com', 'rb'));

    var_dump($fp);

    yield sleep(1);
    echo "And we're done after ".(microtime(true)-$t)."\n";

});

