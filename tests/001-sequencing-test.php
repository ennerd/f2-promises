<?php
require(__DIR__."/../vendor/autoload.php");

use function F2\{defer, sleep, queueMicrotask, readable, writable, await, asserty};

/**
 * This file should test that all functionality is consistent. It does that by logging each intermediate result, and then 
 * a checksum
 */
function f2_log(string $evt, int $order) {
    static $expectedIndex = 0;
    echo "#$order: $evt\n";
    asserty($order == $expectedIndex, "Expected index $expectedIndex, but received $order: ".$evt);
    $expectedIndex++;
}

/*
register_shutdown_function(function() {
    // Ensure that we're the last function to happen
    register_shutdown_function(function() {
        foreach ($GLOBALS['F2_LOG'] as $l
        print_r($GLOBALS['F2_LOG']);
    });
});
*/

queueMicrotask(function() {
    f2_log("Microtask before all defers", 0);
    defer(function($message, $pos) {
        f2_log($message, $pos);
    }, "Deferred from first microtask with arguments", 50);
    sleep(0.5)->then(function() {
        f2_log("Slept for 0.5", 55);
    });
    sleep(0.1)->then(function() {
        f2_log("Slept for 0.1", 54);
    });
    yield sleep(0.5);
    f2_log("Slept also for 0.5, but after the first", 56);
});

defer(function() {
    f2_log("First defer", 2);
    queueMicrotask(function() { 
        f2_log("Microtask inside first defer", 3); 
        queueMicrotask(function() {
            f2_log("Microtask from microtask", 4);
        });
    });
})->then(function($result) {
    f2_log("Then handler for first defer", 5);
});

defer(function() {
    f2_log("Second defer", 6);
    defer(function() {
        f2_log("Defer inside second defer", 51);
        yield defer(function() {
            f2_log("Yielded defer inside second inner defer", 52);
        });
        f2_log("Second defer inside second defer", 53);
    });
});

queueMicrotask(function() {
    f2_log("Microtask queued after all defers", 1);
});

for ($i = 7; $i < 50; $i++) {
    defer(function() use ($i) {
        f2_log("Defered task #$i", $i);
    });
}
