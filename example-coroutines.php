<?php
require('vendor/autoload.php');

use function F2\{defer, readable, writable, sleep};

$t = microtime(true);

/**
 * Create a coroutine
 */
defer(function() {
    echo t()."Hello from the CoRoutine!\n";

    // instead of using ->then($callback) - you can just yield
    for ($i = 0; $i < 100; $i++) {
        yield sleep(0.01);
    }

    echo t()."Slept for approximately 1 seconds\n";
});

/**
 * Create another coroutine that writes a file
 */
defer(function() {
    echo t()."A coroutine using file I/O!\n";

    // The 'n' modifier is undocumented and may not work on all platforms
    $fp = yield writable(fopen('/tmp/some-file.txt', 'wbn'));

    for ($i = 2000; $i >= 1; $i--) {
        fwrite( yield readable($fp), "$i bottles of beer on the wall, $i bottles of beer...\n" );
    }

    echo t()."Done writing the file!\n";
}).then(function() {
    unlink("/tmp/some-file.txt");
});

function t() {
    global $t;

    return round((microtime(true) - $t) * 1000)." ms ";
}

/**
Output:

0 ms Hello from the CoRoutine!
0 ms A coroutine using file I/O!
38 ms Done writing the file!
1001 ms Slept for approximately 1 seconds
*/
