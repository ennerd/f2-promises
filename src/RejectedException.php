<?php
declare(strict_types=1);

namespace F2\Promise;

class RejectedException extends Exception {}
