<?php
declare(strict_types=1);

namespace F2\Promise;

class FulfilledPromise extends Promise {

    public function __construct($value) {
        if (method_exists($value, 'then')) {
            throw new \InvalidArgumentException(
                'You cannot create a FulfilledPromise with a promise.');
        }

        $this->f2_fulfill($value);
    }

}
