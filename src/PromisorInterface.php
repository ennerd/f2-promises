<?php
declare(strict_types=1);

namespace F2\Promise;

interface PromisorInterface {
    /**
     * Returns a promise.
     *
     * @return PromiseInterface
     */
    public function promise(): ThenableInterface;
}
