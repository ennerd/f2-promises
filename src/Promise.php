<?php
declare(strict_types=1);

namespace F2\Promise;

use function F2\queueMicrotask;

class Promise implements PromiseInterface {
    use ThenableTrait;

    static $seq = 0;

    public function __construct(callable $waitFunction=null, callable $cancelFunction=null) {
        $creator = current(debug_backtrace(0, 1));
        $this->_ident = static::$seq++.":".$creator['file'].':'.$creator['line'];

        if ($cancelFunction) {
            $this->_cancelFunction = $cancelFunction;
        }
        if ($waitFunction) {
            $self = $this;
            queueMicrotask(function() use ($self, $waitFunction) {
                $waitFunction([$self, 'resolve'], [$self, 'reject']);
            });
        }
    }

}
