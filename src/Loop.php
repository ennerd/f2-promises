<?php
declare(strict_types=1);

namespace F2\Promise;

use Generator, Closure;
use function call_user_func, call_user_func_array, array_shift, method_exists,
    register_shutdown_function, array_map, usleep, stream_select, array_merge,
    ftell, array_key_exists, stream_get_meta_data, stream_is_local;

/**
 * Internal class used as the backend for the public API made available in
 * functions.php
 */
class Loop {

    const ACTION = 'action';
    const ARGS = 'args';
    const SUCCESS = 'success';
    const FAILURE = 'failure';
    const GENERATOR_SEND = 'generator_send';
    const GENERATOR_THROW = 'generator_throw';
    const STREAM = 'stream';

    protected $deferWith;
    protected $microtasks = [];
    protected $tasks = [];
    protected $tasksLength = 0;
    protected $readables = [];
    protected $writables = [];
    protected $pcntlCountdown = 0;
    protected $pcntl;
    protected $pcntlPoll;
    protected $isWatchingStreams = false;
    protected $lastStreamWatch = null;
    protected $options = [
        'usleep_time' => 500,
        'debug' => false,
        ];

    public function __construct(callable $defer=null, array $options=[]) {
        $this->options = $options + $this->options;
        if ($defer !== null) {
            $this->deferWith = $defer;
        } else {
            register_shutdown_function(Closure::fromCallable( [$this, 'run'] ));
        }
        $this->pcntl = \function_exists('pcntl_signal') && \function_exists('pcntl_signal_dispatch');
        $this->pcntlPoll = $this->pcntl && !\function_exists('pcntl_signal_dispatch');

        if ($this->pcntl && !$this->pcntlPoll) {
            \pcntl_async_signals(true);
        }
    }

    public function cancelReadable($stream, callable $callable=null) {
        $id = (int) $stream;
        if (!isset($this->readables[$id])) {
            return;
        }
        if ($callable) {
            foreach ($this->readables[$id]['callables'] as $k => $thatCallable) {
                unset($this->readables[$id]['callables'][$k]);
                if (sizeof($this->readables[$id]['callables'])===0) {
                    unset($this->readables[$id]);
                }
            }
        } else {
            unset($this->readables[$id]);
        }
    }

    public function whenReadable($stream, callable $callable) {
        $id = (int) $stream;
        if (!isset($this->readables[$id])) {
            $this->readables[$id] = [ 'stream' => $stream, 'callables' => [] ];
        }
        $this->readables[$id]['callables'][] = $callable;
        $this->watchStreams();
    }

    public function cancelWritable($stream, callable $callable=null) {
        $id = (int) $stream;
        if (!isset($this->writables[$id])) {
            return;
        }
        if ($callable) {
            foreach ($this->writables[$id]['callables'] as $k => $thatCallable) {
                unset($this->writables[$id]['callables'][$k]);
                if (sizeof($this->writables[$id]['callables'])===0) {
                    unset($this->writables[$id]);
                }
            }
        } else {
            unset($this->writables[$id]);
        }
    }

    public function whenWritable($stream, callable $callable) {
        $id = (int) $stream;
        if (!isset($this->writables[$id])) {
            $this->writables[$id] = [ 'stream' => $stream, 'callables' => [] ];
        }
        $this->writables[$id]['callables'][] = $callable;
        $this->watchStreams();
    }

    public function queueMicrotask(callable $callable, array $args=[]) {
        if ($this->options['debug']) {
            echo "queueMicrotask(".self::describeClosure($callable).")\n";
        }
        $this->microtasks[] = [ self::ACTION => $callable, self::ARGS => $args ];
    }

    public function defer(callable $callable, array $args=[], callable $success=null, callable $failure=null) {
        if ($this->options['debug']) {
            echo "defer(".self::describeClosure($callable).")\n";
        }
        $this->tasks[$this->tasksLength++] = [ self::ACTION => $callable, self::ARGS => $args, self::SUCCESS => $success, self::FAILURE => $failure ];

        // For each task added to the queue, ensure that one tick is performed on our queue
        $this->delegateTick();
    }

    protected function delegateTick() {
        if ($this->deferWith) {
            call_user_func($this->deferWith, Closure::fromCallable([ $this, 'tick' ]));
        }
    }

    public function run() {
        if ($this->deferWith) {
            throw new Exception("The event loop has been delegated. Can't do run().");
        }
        while ($this->tick());
    }

    protected function runMicrotasks() {
        if ($this->options['debug']) {
            echo "runMicrotasks():\n";
            echo "----------------\n";
            foreach ($this->microtasks as $k => $task) {
                echo "$k => ".self::describeClosure($task[self::ACTION])."\n";
            }
            echo "----------------\n";
        }

        // Foreach might be faster, but new tasks might come in while this is running
        while ($task = array_shift($this->microtasks)) {
            if ($this->options['debug']) {
                echo "- task: ".self::describeClosure($task[self::ACTION])."\n";
            }
            $generator = call_user_func_array($task[self::ACTION], $task[self::ARGS]);
            if ($generator instanceof \Generator) {
                /**
                 * Microtasks that yield are moved to ordinary tasks. This means they will continue
                 * running alongside other deferred tasks. Since microtasks are expected to run before
                 * other tasks, we'll call $generator->valid() here. This makes the generator start.
                 */
                $generator->valid();
                $task = [
                    self::ACTION => $generator,
                    self::ARGS => [],
                    self::SUCCESS => function($result) use ($task) {
                        $task[self::GENERATOR_SEND] = $result;
                    },
                    self::FAILURE => function($reason) use ($task) {
                        $task[self::GENERATOR_THROW] = $reason;
                    }
                ];

                $this->tasks[$this->tasksLength++] = $task;

                $this->delegateTick();
            }
        }
    }

    public function tick(): bool {
        $this->runMicrotasks();

        $task = array_shift($this->tasks);
        $this->tasksLength--;

        if ($task === null) {
            return false;
        }

        if ($this->pcntlPoll && $this->pcntlCountdown-- <= 0) {
            \pcntl_signal_dispatch();
            $this->pcntlCountdown = $this->tasksLength;
        }

        try {
            if ($task[self::ACTION] instanceof Generator) {
                // Do we have a value or an exception for the generator?
                if (array_key_exists(self::GENERATOR_SEND, $task)) {
                    $task[self::ACTION]->send($task[self::GENERATOR_SEND]);
                    unset($task[self::GENERATOR_SEND]);
                } elseif (array_key_exists(self::GENERATOR_THROW, $task)) {
                    $task[self::ACTION]->throw($task[self::GENERATOR_THROW]);
                    unset($task[self::GENERATOR_THROW]);
                }

                // Does the generator have more work to do?
                if (!$task[self::ACTION]->valid()) {
                    if ($task[self::SUCCESS]) {
                        $this->queueMicrotask($task[self::SUCCESS], [$task[self::ACTION]->getReturn()]);
                    }
                } else {
                    $yielded = $task[self::ACTION]->current();

                    if (is_resource($yielded)) {
                        $promise = new Promise();
                        $stream = $yielded;
                        // We'll automatically watch this stream if we can
                        $md = stream_get_meta_data($stream);
                        if (!$md['blocked']) {
                            $promise->reject(new CoroutineException("No point in waiting for non-blocking streams. Please use stream_set_blocking()."));
                        } else {
                            // Infer the optimal resolution
                            $readable = null; // Undetermined
                            $writable = null; // Undetermined
                            if (strpos($md['mode'], '+') !== false) {
                                $readable = true;
                                $writable = true;
                            } elseif (strpos($md['mode'], 'r') !== false) {
                                $readable = true;
                                $writable = false;
                            } else {
                                $readable = false;
                                $writable = true;
                            }
                            if ($readable && $writable) {
                                /**
                                 * Developer yielded a stream that is both readable and writable. We'll wait until it is both readable
                                 * and writable. This might not be optimal, but it makes the documentation simple. The optimal solution
                                 * in this case would be to "yield F2\readable($fp)" or "yield F2\writable($fp)" depending on what's
                                 * needed.
                                 */
                                $checks = 2;
                                $solver = function() use ($promise, $stream, &$checks, &$solver) {
                                    if (--$checks === 0) {
                                        $promise->resolve($stream);
                                        $this->cancelReadable($stream, $solver);
                                        $this->cancelWritable($stream, $solver);
                                    }
                                };
                                $this->whenReadable($stream, $solver);
                                $this->whenWritable($stream, $solver);
                            } elseif ($readable) {
                                $this->whenReadable($stream, function() use ($promise, $stream) {
                                    $promise->resolve($stream);
                                });
                            } elseif ($writable) {
                                $this->whenWritable($stream, function() use ($promise, $stream) {
                                    $promise->resolve($stream);
                                });
                            } else {
                                $promise->reject(new CoroutineException("Unable to determine if stream is readable or writable"));
                            }
                        }

                        // We'll insert this promise instead of the stream
                        $yielded = $promise;
                    }

                    // Is it a thenable?
                    if (method_exists($yielded, 'then')) {
                        $yielded->then(function($result) use ($task, $yielded) {
                            $task[self::GENERATOR_SEND] = $result;
                            $this->tasks[$this->tasksLength++] = $task;
                            $this->delegateTick();
                        }, function($result) use ($task) {
                            if (!($result instanceof \Throwable)) {
                                $result = new RejectedException($result);
                            }
                            $task[self::GENERATOR_THROW] = $result;
                            $this->tasks[$this->tasksLength++] = $task;
                            $this->delegateTick();
                        });
                    } else {
                        $task[self::GENERATOR_SEND] = $yielded;
                        $this->tasks[$this->tasksLength++] = $task;
                        $this->delegateTick();
                    }
                }
            } else {
                $result = call_user_func_array($task[self::ACTION], $task[self::ARGS]);

                if ($result instanceof Generator) {
                    $task[self::ACTION] = $result;
                    $task[self::ARGS] = null;
                    $this->tasks[$this->tasksLength++] = $task;
                    $this->delegateTick();
                } elseif ($task[self::SUCCESS]) {
                    $this->queueMicrotask($task[self::SUCCESS], [$result]);
                }
            }
        } catch (\Exception $e) {
            // Throwables are internal PHP errors, so we won't hide them
            if ($task[self::FAILURE]) {
                $this->queueMicrotask($task[self::FAILURE], [$e]);
            } else {
                throw $e;
            }
        }
        $this->runMicrotasks();

        /**
         * Avoiding busy waiting
         *
         * - When using another event loop, it is that event loops responsibility to yield CPU time to the OS.
         * - When we have multiple tasks remaining, then we should finish them as fast as possible.
         * - When we're using stream_select, then stream_select will yield CPU time to the OS.
         */
        if ($this->deferWith === null && $this->tasksLength < 4 && !$this->isWatchingStreams) {
            usleep($this->options['usleep_time']);
        }

        if ($this->tasksLength === 0 && sizeof($this->microtasks) > 0) {
            // Defer a null operation, because we need microtasks to finish up
            $this->defer(function() {});
        }

        return $this->tasksLength > 0;
    }

    protected function watchStreams() {
        if ($this->isWatchingStreams) {
            return;
        }
        $this->defer(Closure::fromCallable( [$this, 'streamWatcher'] ));
    }

    protected function streamWatcher() {
        if ($this->lastStreamWatch !== null && microtime(true) - $this->lastStreamWatch < 0.001) {
            $usleep = $this->options['usleep_time'];
        } else {
            $usleep = 0;
        }

        $readables = array_map(function($e) { return $e['stream']; }, $this->readables);
        $writables = array_map(function($e) { return $e['stream']; }, $this->writables);

        $count = static::streamSelect($readables, $writables, 0, $usleep);

        foreach ($readables as $stream) {
            $id = (int) $stream;
            foreach ($this->readables[$id]['callables'] as $callable) {
                $this->queueMicrotask($callable);
            }
            unset($this->readables[$id]);
        }

        foreach ($writables as $stream) {
            $id = (int) $stream;
            foreach ($this->writables[$id]['callables'] as $callable) {
                $this->queueMicrotask($callable);
            }
            unset($this->writables[$id]);
        }

        if (sizeof($this->readables) > 0 || sizeof($this->writables) > 0) {
            // We didn't finish with all streams, so schedule us to keep on
            $this->defer(Closure::fromCallable( [$this, 'streamWatcher'] ));
        } else {
            $this->isWatchingStreams = false;
        }
    }

    // Function shamelessly "inspired" from ReactPHP
    protected static function streamSelect(array &$read, array &$write, int $tv_sec, int $tv_usec = 0) {
        if ($read || $write) {
            // We do not usually use or expose the `exceptfds` parameter passed to the underlying `select`.
            // However, Windows does not report failed connection attempts in `writefds` passed to `select` like most other platforms.
            // Instead, it uses `writefds` only for successful connection attempts and `exceptfds` for failed connection attempts.
            // We work around this by adding all sockets that look like a pending connection attempt to `exceptfds` automatically on Windows and merge it back later.
            // This ensures the public API matches other loop implementations across all platforms (see also test suite or rather test matrix).
            // Lacking better APIs, every write-only socket that has not yet read any data is assumed to be in a pending connection attempt state.
            // @link https://docs.microsoft.com/de-de/windows/win32/api/winsock2/nf-winsock2-select
            $except = null;
            if (\DIRECTORY_SEPARATOR === '\\') {
                $except = array();
                foreach ($write as $key => $socket) {
                    if (!isset($read[$key]) && @\ftell($socket) === 0) {
                        $except[$key] = $socket;
                    }
                }
            }
            // suppress warnings that occur, when stream_select is interrupted by a signal
            $ret = @stream_select($read, $write, $except, $tv_sec, $tv_usec);
            if ($except) {
                $write = array_merge($write, $except);
            }
            return $ret;
        }
        if ($tv_usec > 0 || $tv_sec > 0) {
            usleep($$tv_usec + $tv_sec * 1000000);
        }
        return 0;
    }

    protected static function describeClosure(callable $callable): string {
        $ref = new \ReflectionFunction($callable);
        $fn = $ref->getFileName();
        $l = strlen($fn);
        for ($i = 1; $i < $l; $i++) {
            if (substr($fn, 0, $i) !== substr(__DIR__, 0, $i)) {
                $fn = substr($fn, $i-1);
                break;
            }
        }
        return "<Closure ".$fn.":".$ref->getStartLine().">";
    }

}
