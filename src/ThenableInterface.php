<?php
namespace F2\Promise;

interface ThenableInterface {
    public function then(callable $onFulfilled = null, callable $onRejected = null): ThenableInterface;
}
