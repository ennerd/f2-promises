<?php
declare(strict_types=1);

namespace F2\Promise;

use function F2\{queueMicrotask};
use function method_exists, array_shift, trigger_error, get_class, is_scalar, var_export, json_encode;
/**
 * This trait implements the minimum interface required by promises except
 * that from Amp. It is supposed to be implemented in a "Super Promise" class
 * that hopefully will be compatible with everything.
 */
trait ThenableTrait {
    /**
     * Prefixed properties to avoid collisions with other implementations
     */
    protected $_result = null;
    protected $_state = PromiseInterface::PENDING;
    protected $_fulfilledCallbacks = [];
    protected $_rejectedCallbacks = [];
    protected $_alwaysCallbacks = [];
    protected $_cancelFunction = null;
    protected $_ident = null;

    /**
     * @param callable $onFulfilled
     * @param callable $onRejected
     */
    public function then(callable $onFulfilled=null, callable $onRejected=null): ThenableInterface {
//echo $this->f2_ident()."->then()\n";
        if ($this->_state === PromiseInterface::FULFILLED && $onFulfilled) {
            // This promise is fulfilled, so the jobs are done immediately
            queueMicrotask($onFulfilled, $this->_result);
        } elseif ($this->_state === PromiseInterface::REJECTED && $onRejected) {
            // This promise is fulfilled, so the jobs are done immediately
            queueMicrotask($onRejected, $this->_result);
        } else {
            if (is_callable($onFulfilled)) {
                $this->_fulfilledCallbacks[] = $onFulfilled;
            }
            if (is_callable($onRejected)) {
                $this->_rejectedCallbacks[] = $onRejected;
            }
        }
        return $this;
    }

    public function getState(): string {
        return $this->_state;
    }

    public function otherwise(callable $onRejected) {
        return $this->then(null, $onRejected);
    }

    public function always(callable $onAlways) {
        $this->_alwaysCallbacks[] = $onAlways;
        return $this;
    }

    public function resolve($result) {
//echo $this->f2_ident()."->resolve()\n";
        if ($this->_state === PromiseInterface::FULFILLED && $this->_result === $result) {
            return;
        }
        $this->f2_assertPending();

        $wasHandled = false;
        $this->_state = PromiseInterface::FULFILLED;
        $this->_result = $result;
        if (method_exists($result, 'then')) {
            // This will be fulfilled when that promise is fulfilled
            while (null !== ($callback = array_shift($this->_fulfilledCallbacks))) {
                $wasHandled = true;
                $result->then($callback);
            }
            while (null !== ($callback = array_shift($this->_rejectedCallbacks))) {
                $wasHandled = true;
                $result->then(null, $callback);
            }
            while (null !== ($callback = array_shift($this->_alwaysCallbacks))) {
                $wasHandled = true;
                if (method_exists($result, 'always')) {
                    $result->always($callback);
                } else {
                    $result->then($callback, $callback);
                }
            }
        } else {
            $this->_result = $result;
            while (null !== ($callback = array_shift($this->_fulfilledCallbacks))) {
                $wasHandled = true;
                queueMicrotask($callback, $result);
            }
            // Remove any rejected callbacks, because we don't need memory leaks.
            $this->_rejectedCallbacks = [];
            if ($this->f2_doAlways()) {
                $wasHandled = true;
            }
        }

        if (!$wasHandled) {
            $this->f2_debug("Promise silently resolved", $result);
        }
    }

    public function reject($reason) {
//echo $this->f2_ident()."->reject()\n";
        if ($this->_state === PromiseInterface::REJECTED && $this->_result === $reason) {
            // Guzzle ignores this
            return;
        }
        $this->f2_assertPending();
        $this->_state = PromiseInterface::REJECTED;
        $wasHandled = false;
        while (null !== ($callback = array_shift($this->_rejectedCallbacks))) {
            queueMicrotask($callback, $reason);
            $wasHandled = true;
        }
        if (!$wasHandled) {
            // Unhandled exceptions needs to be taken care of!
            if ($reason instanceof \Throwable) {
                throw $reason;
            }
        }
        $this->_fulfilledCallbacks = [];
        $this->f2_doAlways();
    }

    public function cancel() {
//echo $this->f2_ident()."->cancel()\n";
        if ($this->_state !== PromiseInterface::PENDING) {
            // Guzzle ignores the cancel request, if the state is resolved
            return;
        }

        if ($this->_cancelFunction) {
            $fn = $this->_cancelFunction;
            $this->_cancelFunction = null;
            try {
                $fn();
            } catch (\Exception $e) {
                // Throwables are internal PHP errors, so we won't hide them
                $this->reject($e);
            }
        }

        if ($this->_state === PromiseInterface::PENDING) {
            $this->reject(new CancellationException('Promise has been cancelled'));
        }
    }

    /**
     * Method is private because it should not be used by implementations, and
     * will instead be invoked automatically when calling fulfull() or reject()
     */
    private function f2_doAlways() {
//echo $this->f2_ident()."->f2_doAlways()\n";
        $wasHandled = false;
        while (null !== ($callback = array_shift($this->_alwaysCallbacks))) {
            queueMicrotask($callback);
            $wasHandled = true;
        }
        return $wasHandled;
    }

    private function f2_assertPending() {
//echo $this->f2_ident()."->f2_assertPending()\n";
        if ($this->_state !== PromiseInterface::PENDING) {
            throw new \LogicException("Promise is already resolved");
        }
    }

    private function f2_ident() {
        return ($this->_ident ?? "<Promise>")."(".$this->_state.")";
    }

    public function __toString() {
        return $this->f2_ident();
    }

    protected function f2_debug(string $message, $value=null) {
        if ($value instanceof \Throwable) {
            $message .= ": ".get_class($value).": ".$value->getMessage();
        } elseif (is_scalar($value)) {
            $message .= ": (value=".trim(var_export($value, true)).")";
        } elseif ($value !== null) {
            try {
                if (function_exists('json_encode')) {
                    $s = json_encode($value);
                } else {
                    $s = var_export($value, true);
                }
                if (strlen($s) > 50) {
                    $s = substr($s, 0, 45)."(...)";
                }
                $message .= ": (value=$s)";
            } catch (\Throwable $e) {
                // Value is probably not serializable
                $message .= ": (value=[unserializable])";
            }
        } else {
            $message .= ': (value=null)';
        }
        // I suggest you capture and process this
//        trigger_error($message, E_USER_NOTICE);
    }
}
