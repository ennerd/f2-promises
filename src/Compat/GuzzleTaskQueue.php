<?php
namespace F2\Promises\Compat;

use function F2\Promises\defer;
use function F2\Promises\tick;

class GuzzleTaskQueue implements \GuzzleHttp\Promise\TaskQueueInterface {

    public function __construct() {
    }

    public function isEmpty() {
        // Guzzle should not care if the queue is empty
        throw new \Exception("Guzzle should not care if the queue is empty.");
        return false;
    }

    public function add(callable $task) {
var_dump(debug_backtrace(false, 10));
        defer($task);
    }

    public function run() {
        throw new \Exception("Guzzle is not responsible for ensuring that the queue is running");
    }
}
