<?php
declare(strict_types=1);

/**
 * A minimalist asynchronous PHP environment. Libraries that are developed to utilize f2/promise will
 * work seamlessly with other async frameworks, and can also be used within non-async processing models.
 */
namespace F2;

//require(__DIR__.'/internal.php');

use F2\Promise\Loop;
use F2\Promise\Promise;
use F2\Promise\PromiseInterface;
use F2\Promise\Exception;

/**
 * To use a different event loop implementation, simply provide a defer callable
 * that will schedule our callables and another that will execute one deferred
 * function.
 */
function setup(callable $defer) {
    backend($defer);
}

/**
 * Run the function on the next tick. Returns a promise containing the
 * result of the called function. The functions will be run using the default
 * event loop.
 */
function defer(callable $coroutine, ...$args): PromiseInterface {
    $promise = new Promise();
    backend()->defer($coroutine, $args, function($result) use ($promise) {
        $promise->resolve($result);
    }, function($error) use ($promise) {
        $promise->reject($error);
    });
    return $promise;
}

/**
 * Run the function immediately before the next tick or immediately
 * after the current tick. Similar to javascript queueMicrotask().
 */
function queueMicrotask(callable $callable, ...$args) {
    backend()->queueMicrotask($callable, $args);
}

/**
 * Sleep for the specified number of seconds (decimals allowed)
 */
function sleep(float $time): PromiseInterface {
    $time += microtime(true);
    $promise = new Promise();
    defer(function() use ($time, $promise) {
        while (($newTime = microtime(true)) < $time) {
           yield;
        }
        $promise->resolve($time);
    });
    return $promise;
}

/**
 * Wait until reading this stream will not block
 */
function readable($stream) {
    $promise = new Promise();
    backend()->whenReadable($stream, function() use($promise, $stream) {
        $promise->resolve($stream);
    });
    return $promise;
}

/**
 * Wait until writing this stream will not block
 */
function writable($stream) {
    $promise = new Promise();
    backend()->whenWritable($stream, function() use($promise, $stream) {
        $promise->resolve($stream);
    });
    return $promise;
}

/**
 * Wait until the promise has resolved, and run tick() while waiting. This only works if the promises are being resolved
 * by callbacks deferred with F2\defer() or F2\queueMicrotask().
 *
 * @param PromiseInterface $promise The promise to wait for
 */
function await($thenable) {
    if (!method_exists($thenable, 'then')) {
        throw new Exception("Can't only await thenables.");
    }
    $value = null;
    $state = 0;
    $promise->then(function($result) use (&$value, &$state) {
        $value = $result;
        $state = 1;
    }, function($reason) use (&$value, &$state) {
        $value = $reason;
        $state = 2;
    });

    while ($state === 0) {
        if (!backend()->tick()) {
            throw new Exception("Promise can't be resolved.");
        }
    }

    if ($state === 1) {
        return $value;
    } else {
        if ($value instanceof \Throwable) {
            throw $value;
        }
        throw new Promise\RejectedException($value);
    }
}

/**
 * Set or return the event loop backend
 *
 * @param Loop $setBackend
 */
function backend(Loop $setBackend=null): Loop {
    static $backend;
    if ($setBackend) {
        if ($backend) {
            throw new Exception("Too late to set backend. The backend is already in use.");
        }
        $backend = $setBackend;
    } elseif (!$backend) {
        $backend = new Loop(null, ['usleep_time' => 1000]);
    }
    return $backend;
}

