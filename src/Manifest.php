<?php
declare(strict_types=1);

namespace F2\Promise;

use F2\Common\AbstractManifest;

class Manifest extends AbstractManifest {

    public function getDependencies(): iterable {
        return [
            \F2\Common\Manifest::class,
        ];
    }

    public function getF2Methods(): iterable {
        yield "defer"  => 'F2\\defer';
        yield "queueMicrotask" => 'F2\queueMicrotask';
        yield "sleep" => 'F2\\sleep';
        yield "readable" => 'F2\\readable';
        yield "writable" => 'F2\\writable';
    }

}
