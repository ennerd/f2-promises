<?php
declare(strict_types=1);

namespace F2\Promise;

interface PromiseInterface extends ThenableInterface {
    const PENDING = 'pending';
    const FULFILLED = 'fulfilled';
    const REJECTED = 'rejected';

    public function getState(): string;
}
